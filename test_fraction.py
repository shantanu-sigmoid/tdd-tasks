import pytest
import unittest
from unittest import mock
from unittest.mock import patch,MagicMock
from fraction import FractionCalculator

# 1. Put the functions under a class 
# 2. Use mock for input file.   


class TestClass(unittest.TestCase):
    def setUp(self):
        print("Setup")
        self.frc_no_value_passed = FractionCalculator()
        self.frc_one_value_passed = FractionCalculator(1)
        self.frc_one_value_specified_with_variable = FractionCalculator(den = 10)
        self.frc_both_value_passed = FractionCalculator(-1, 2)
        self.frc_half = FractionCalculator(1, 2)
        self.frc_one_third = FractionCalculator(1, 3)
        self.frc_default = FractionCalculator()
    def tearDown(self):
        print("TearDown")

    # Instantiate the Class
    def test_can_instantiate_fraction(self):
        assert self.frc_no_value_passed
        assert self.frc_one_value_passed
        assert self.frc_one_value_specified_with_variable
        assert self.frc_both_value_passed

    # Test gcd method in FractionCalculator class
    def test_gcd_method(self):
        self.assertEqual(self.frc_half.gcd(1, 4), 1)
        self.assertRaises(AssertionError, self.frc_half.gcd, 2.0, 8.0)
        self.assertRaises(AssertionError, self.frc_half.gcd, -1, 4)

    # Test lowest_method
    def test_lowest_method(self):
        assert self.frc_default.lowest(30, 70, 1) == (3, 7)
        with self.assertRaises(AssertionError):
            self.frc_default.lowest(4.0, 8.0, 1)

    # Test add_fraction method
    def test_add_fraction_method(self):
        self.assertEqual(self.frc_half.addFraction(self.frc_one_third), (5, 6))
    
    # Test if input can be read from file
    def test_read_from_file_for_gcd(self):
        contents = self.frc_default.get_test_cases("input_file.txt")
        assert contents == [[1,2,3,4],[4,5,5,6],[1,23,4,4]]

    # Test to check lowest if can be called from addFraction
    def test_lowest_called_check(self):
        ad_frac = FractionCalculator()
        ad_frac.lowest = MagicMock()
        ad_frac.addFraction(self.frc_default)
        ad_frac.lowest.assert_called_once_with(2,1,1)