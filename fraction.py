import csv,os
# Rename class to FractionCalculator and file appropriately --done

class FractionCalculator:
    def __init__(self, num = 1, den = 1):
        self.num = abs(num) if num*den >=0 else -abs(num)
        self.den = abs(den)
    
    # Function to return gcd of a(int) and b(int)
    def gcd(self, a, b):
        assert type(a) == int
        assert type(b) == int
        assert a >= 0
        assert b >= 0
        if (a == 0):
            return b
        return self.gcd(b % a, a)

    # Function to convert the obtained
    # fraction into it's simplest form
    def lowest(self, num, den, sign):
        # Finding gcd of both terms
        common_factor = self.gcd(num, den)
        # Converting both terms
        # into simpler terms by
        # dividing them by common factor
        num = int(num / common_factor)
        den = int(den / common_factor)
        return (sign*num, den)

    # Function to add two fractions
    def addFraction(self, frcObject):
        # Finding LCM of den and anotherObject.den
        # LCM = a * b / gcd(a, b)
        den = int((self.den * frcObject.den) / self.gcd(self.den, frcObject.den))
        # Changing the fractions to
        # have same denominator Numerator
        # of the final fraction obtained
        num = int(((self.num) * (den / self.den) + (frcObject.num) * (den / frcObject.den)))
        # Calling function to convert
        # final fraction into it's
        # simplest form
        sign = -1 if den*num < 0 else 1
        return self.lowest(abs(num), abs(den), sign)

    # Takes input of two fractions a,b,p,q  where a/b if frc1 and p/q is frc2
    def get_test_cases(self, path):
        with open(path) as f:
            doc = csv.reader(f, delimiter=',')
            retVal = []
            for row in doc:
                new_row = []
                for col in row:
                    if("." in col):
                        new_row.append(float(col))
                    else:
                        new_row.append(int(col))
                retVal.append(new_row)
            return retVal


if __name__ == "__main__":
    # Driver Code
    frc = FractionCalculator()
    arr = frc.get_test_cases("input_file.txt")
    for i, j, p, q in arr:
        frc1 = FractionCalculator(i,j)
        frc2 = FractionCalculator(p,q)
        print(frc1.addFraction(frc2))
